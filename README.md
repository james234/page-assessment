## Description

Wallet System API

## Step 1

```bash
# Clone Project from gitlab
$ git clone https://gitlab.com/james234/fincra-support-system.git

```

## Step 2

```bash

# Install Project Dependencies
$ Composer Install

```

## Step 3

```bash

# Run Project
$ php artisan serve
```

## Documentation

-   Project Documentation can be accessed using the state link - <https://documenter.getpostman.com/view/25552330/2s9XxySZQy>

## Stay in touch

-   Author - [James Anih][uchennaanih16@gmail.com]
