<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Artisan; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/migrate', function () {
    Artisan::call('migrate');
    return 'migrated succeful';
});


Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);


Route::get('/users', [UserController::class, 'index']);
Route::get('/wallets', [UserController::class, 'getAllWallets']);


Route::get('/summaries', [UserController::class, 'countSummary']); # return a summary of all users, wallets counts

Route::get('/wallet/{walletID}', [UserController::class, 'walletUserTransactionByWalletId']); # return wallet details alongside with the owner, transactions


Route::get('/wallets/type', [UserController::class, 'getWalletType']);# return a list of all wallet types

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/create/wallet', [UserController::class, 'createWallet']);
    Route::post('/wallet/transfer', [UserController::class, 'walletToWallet']);
    Route::get('/user-wallet-transaction', [UserController::class, 'userWalletTransaction']);
    Route::post('wallet/fund', [UserController::class, 'fundWallet']);
    Route::post('/user/set-pin', [UserController::class, 'setPin']);
});
